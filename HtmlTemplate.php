<?php

namespace JKFL\Html;

class HtmlTemplate {
	private $variables = array();
	private $html;
	public static string $tagPrefix = "jkfl:";
	public static function startTemplate() {
		ob_start();
		
	}
	public static function endTemplate () {
		$contents = ob_get_contents();
		ob_clean();

		$template = new HtmlTemplate($contents);	

		return $template;
	}
	
	public function __construct($html) {
		$this->html = $html;
	}
	public function addVarMap($name, $mapVal) {
		$this->variables["<" . self::$tagPrefix . $name . "/>"] = $mapVal;
	}
	
	public function toHtml() {
		$output = $this->html;
		
		$output = strtr($output, $this->variables );
		
		return $output;
	}
	public function arrayToHtmlOptions(array $opt, $selectedVal, $bStartWithBlankOption = true, $optGroupLabel = "" ) : string {
		$html = "";

		if ( $bStartWithBlankOption ) {
			$html .= "<option value=\"\"></option>";
		}

		if ( !empty($opt) && $optGroupLabel !== "" ) {
			$html .= "<optgroup label=\"$optGroupLabel\">";
		}
		foreach( $opt as $id => $val) {
			$selected = "";
			if ( is_array($selectedVal) && array_key_exists($id, $selectedVal)) {
				$selected = " selected=\"selected\"";
			}
			else if ( (string)$selectedVal != '' && $id == $selectedVal ) {
				$selected = " selected=\"selected\"";
			}

			$html .= "<option value=\"$id\"$selected>$val</option>";
		}
		if ( !empty($opt) && $optGroupLabel !== "" ) {
			$html .= "</optgroup>";
		}
		return $html;
	}
	
	public function arrayToHtmlRows(array $rows, bool $useKeysAsCellClass = false) : string {
		$html = "";
		foreach($rows as $row) {
			$html .= "<tr>";
			foreach($row as $key => $value ) {
				if ( $useKeysAsCellClass ) {
					$class = str_replace("\"", "", $key);
					$html .= "<td class=\"$class\">$value</td>";
				}
				else {
					$html .= "<td>$value</td>";
				}
			}
			$html .= "</tr>";
		}
		return $html;
	}
}